If you are migrating from an existing pancrypticon instance you can place a
raw or gzipped database dump in this directory and it will be automatically
imported when the database is initialized. The file must have `.sql` or
`.sql.gz` extension. Files with a `.sh` extension will also be executed. All
files are processed in alphanumeric order by filename. See the mariadb docker
documentation for more details.
