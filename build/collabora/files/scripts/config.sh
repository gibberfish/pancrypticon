#!/bin/bash

# escape the dots in the FQDN
escaped=`/bin/echo $NEXTCLOUD_DOMAIN_NAME | /bin/sed 's|\.|\\\\\\\.|g'`

/bin/sed -i "s|NEXTCLOUD_DOMAIN_NAME|$escaped|g" /etc/loolwsd/loolwsd.xml
