#!/bin/bash

/bin/echo $NEXTCLOUD_DOMAIN_NAME > /etc/mailname
/bin/cp /etc/resolv.conf  /var/spool/postfix/etc/
/usr/sbin/postfix start-fg
