#!/bin/bash

/usr/bin/sec --detach --conf=/etc/sec/letsencrypt.rules --input=/etc/letsencrypt/letsencrypt.log

/usr/local/sbin/install.sh && \

# Do not proceed until the database is available
while [ `/bin/nc -z mariadb 3306` ]; do
    /bin/echo "*** Waiting for database container to initialize..."
    /bin/sleep 3
done && \

/bin/mkdir -p /run/ejabberd && \
chown ejabberd.ejabberd /run/ejabberd && \
/usr/sbin/ejabberdctl foreground
