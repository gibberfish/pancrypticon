#!/bin/bash

if [ ! $TESTING ]; then
    # create combined cert
    # copy dhparams
    while [ ! -e /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/privkey.pem ]; do
        /bin/echo " - Waiting for LE cert to generate..."
        /bin/sleep 5
    done
    /bin/cat /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/privkey.pem > \
        /etc/ejabberd/ejabberd.pem && \
    /bin/cat /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/cert.pem >> \
        /etc/ejabberd/ejabberd.pem && \
    /bin/cat /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/chain.pem >> \
        /etc/ejabberd/ejabberd.pem

    # copy dhparams
    while [ ! -e /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/dhparams.pem ]; do
        /bin/echo " - Waiting for dhparams.pem to generate..."
        /bin/sleep 5
    done
    /bin/cp /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/dhparams.pem \
        /etc/ejabberd/
fi

# substitute values from settings.env
/bin/sed -i "s|NEXTCLOUD_DOMAIN_NAME|$NEXTCLOUD_DOMAIN_NAME|g" \
    /etc/ejabberd/ejabberd.yml \
    /etc/xcauth.conf && \
/bin/sed -i "s|OJSXC_TOKEN|$OJSXC_TOKEN|g" /etc/xcauth.conf

# set database credentials, etc
/bin/sed -i "s|MYSQL_USER|$MYSQL_USER|g" /etc/ejabberd/ejabberd.yml && \
/bin/sed -i "s|MYSQL_PASSWORD|$MYSQL_PASSWORD|g" /etc/ejabberd/ejabberd.yml && \
/bin/sed -i "s|LANGUAGE|$LANGUAGE|g" /etc/ejabberd/ejabberd.yml

if [ ! -e /opt/xmpp-cloud-auth ]; then
    cd /opt && \
    /usr/bin/git clone https://github.com/jsxc/xmpp-cloud-auth && \
    cd /opt/xmpp-cloud-auth && \
    ./install.sh

    /bin/chmod 600 /etc/xcauth.conf
fi

/bin/chown ejabberd.ejabberd -R \
    /opt/xmpp-cloud-auth \
    /etc/xcauth.conf \
    /etc/ejabberd
