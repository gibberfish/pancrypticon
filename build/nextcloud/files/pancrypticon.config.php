<?php
$CONFIG = array (
    'datadirectory' => '/opt/nextcloud-data',
    'dbhost' => 'mariadb',
    'dbname' => 'nextcloud',
    'dbpassword' => 'MYSQL_PASSWORD',
    'dbtype' => 'mysql',
    'dbuser' => 'MYSQL_USER',
    'defaultapp' => 'dashboard,files',
    'default_language' => 'LANGUAGE',
    'enable_previews' => true,
    'enabledPreviewProviders' => array(
        'OC\Preview\Image',
    ),
    'filelocking.enabled' => true,
    'htaccess.RewriteBase' => '/',
    'knowledgebaseenabled' => false,
    'log_rotate_size' => 104857600,
    'mail_domain' => 'NEXTCLOUD_DOMAIN_NAME',
    'mail_from_address' => 'no-reply',
    'mail_smtpauth' => false,
    'mail_smtphost' => 'postfix',
    'mail_smtpmode' => 'smtp',
    'mail_smtpport' => '25',
    'memcache.local' => '\OC\Memcache\Redis',
    'memcache.locking' => '\OC\Memcache\Redis',
    'mysql.utf8mb4' => true,
    'overwrite.cli.url' => 'https://NEXTCLOUD_DOMAIN_NAME',
    'overwriteprotocol' => 'https',
    'redis' => array(
        'host' => 'redis',
        'port' => 6379,
    ),
    'remember_login_cookie_lifetime' => 60*60*24*1,
    'skeletondirectory' => '/opt/nextcloud-data/skeleton_files',
    'theme' => 'NEXTCLOUD_THEME',
    'token_auth_enforced' => true,
    'trusted_domains' =>
        array (
            0 => 'NEXTCLOUD_DOMAIN_NAME',
            1 => 'HIDDEN_DOMAIN_NAME',
        ),
    'trusted_proxies' => array('172.18.0.0/24'),
    'updatechecker' => false,
    'upgrade.disable-web' => true,
);
