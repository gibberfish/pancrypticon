#!/usr/bin/env python3

"""
Sets the privacy app's country if not already defined, using a geoip lookup
"""

import os, requests, mysql.connector, geoip2.database

db = mysql.connector.connect(
    host="mariadb",
    user=os.environ["MYSQL_USER"],
    passwd=os.environ["MYSQL_PASSWORD"],
    database="nextcloud",
)
cursor = db.cursor()
cursor.execute(
    "SELECT configvalue FROM oc_appconfig WHERE appid = 'privacy' AND "
    "configkey = 'readableLocation'"
)
row = cursor.fetchone()
if row is None:
    r = requests.get("https://ipv4.nsupdate.info/myip")
    if r.status_code == requests.codes.ok:
        ip = r.text
        reader = geoip2.database.Reader(
            # thanks to the TOS app for making our lives easier 😀
            "/opt/nextcloud/apps/terms_of_service/vendor/GeoLite2-Country.mmdb"
        )
        response = reader.country(ip)
        country_code = response.country.iso_code.lower()
        if len(country_code):
            query = (
                "INSERT INTO oc_appconfig (appid,configkey,configvalue) VALUES "
                "('privacy','readableLocation',%s)"
            )
            cursor.execute(query, (country_code,))
            db.commit()
cursor.close()
db.close()
