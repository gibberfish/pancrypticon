#!/bin/bash

function version { /bin/echo "$@" | /usr/bin/awk -F. '{ printf("%d%03d%03d%03d\n", $1,$2,$3,$4); }'; }

# if this is a new installation, or an older version is currently installed...
if [ ! -f /opt/nextcloud-data/.installed ] || [ ! -f  /opt/nextcloud/version.php ] || \
    [ $(version $NEXTCLOUD_VERSION) -gt $(version $(grep OC_VersionString /opt/nextcloud/version.php | awk -F"'" '{print $2}')) ]; then
    /bin/echo "*** Installing Nextcloud ${NEXTCLOUD_VERSION}..."
    # backup the config
    if [ -f /opt/nextcloud/config/config.php ]; then
        /bin/cp -p /opt/nextcloud/config/config.php /opt/nextcloud-data/config.php.bak
    fi
    cd /opt && \
    # clean any download cruft from previous iterations
    /bin/rm -f nextcloud-*.tar.bz2* nextcloud.asc && \
    ## download and verify nextcloud tarball
    /usr/bin/wget --quiet https://download.nextcloud.com/server/releases/nextcloud-$NEXTCLOUD_VERSION.tar.bz2 && \
    /usr/bin/wget --quiet https://download.nextcloud.com/server/releases/nextcloud-$NEXTCLOUD_VERSION.tar.bz2.sha256 && \
    /usr/bin/wget --quiet https://download.nextcloud.com/server/releases/nextcloud-$NEXTCLOUD_VERSION.tar.bz2.asc && \
    /usr/bin/wget --quiet https://nextcloud.com/nextcloud.asc && \
    /usr/bin/gpg --import -v nextcloud.asc && \
    /usr/bin/gpg --verify -v nextcloud-$NEXTCLOUD_VERSION.tar.bz2.asc nextcloud-$NEXTCLOUD_VERSION.tar.bz2 && \
    /usr/bin/sha256sum --strict -c nextcloud-$NEXTCLOUD_VERSION.tar.bz2.sha256 < nextcloud-$NEXTCLOUD_VERSION.tar.bz2 && \

    # remove the old version
    /bin/rm nextcloud/* -rf && \

    # unpack tarballs
    /bin/tar -xf nextcloud-$NEXTCLOUD_VERSION.tar.bz2 && \
    /usr/bin/touch /opt/nextcloud-data/.ocdata

    if [ -f /opt/nextcloud-data/config.php.bak ]; then
        /bin/cp /opt/nextcloud-data/config.php.bak nextcloud/config/config.php
    fi

    # create the admin's initial files folder
    if [ ! -d /opt/nextcloud-data/admin/files ]; then
      /bin/mkdir -p -m 750 /opt/nextcloud-data/admin/files && \
      /bin/cp -prv /opt/nextcloud-data/skeleton_files/* \
          /opt/nextcloud-data/admin/files
    fi

    # enforce permissions
    /usr/bin/find /opt/nextcloud* -not -user www-data -not -group www-data \
        -exec /bin/chown -R www-data:www-data {} \; && \
    /usr/bin/find /opt/nextcloud* -type d -not -perm 750 \
        -exec /bin/chmod 750 {} \; && \
    /usr/bin/find /opt/nextcloud* -type f -not -perm 640 \
        -exec /bin/chmod 640 {} \;
fi

# copy custom themes and configs
/bin/cp -pr /usr/local/share/themes/* /opt/nextcloud/themes/ && \
/bin/cp /usr/local/etc/pancrypticon.config.php /opt/nextcloud/config/ && \
/bin/mkdir -p /opt/nextcloud-data/news/config && \
/bin/cp /tmp/news_appconfig.ini /opt/nextcloud-data/news/config/config.ini && \
/bin/cp -pr /tmp/newsfeeds /opt/nextcloud-data/ && \
/bin/cp /tmp/mimetypemapping.json /opt/nextcloud/config/ && \
# /bin/cp -pr /tmp/core/templates/filetemplates /opt/nextcloud/core/templates/ && \

# substitute values from settings.env
/bin/sed -i "s|NEXTCLOUD_DOMAIN_NAME|$NEXTCLOUD_DOMAIN_NAME|g" \
    /opt/nextcloud/config/pancrypticon.config.php && \
/bin/sed -i "s|NEXTCLOUD_DOMAIN_NAME|$NEXTCLOUD_DOMAIN_NAME|g" \
    /opt/nextcloud-data/news/config/config.ini && \
/bin/sed -i "s|NEXTCLOUD_THEME|$NEXTCLOUD_THEME|g" \
    /opt/nextcloud/config/pancrypticon.config.php && \
/bin/sed -i "s|LANGUAGE|$LANGUAGE|g" \
    /opt/nextcloud/config/pancrypticon.config.php && \
/bin/sed -i "s|MYSQL_USER|$MYSQL_USER|g" \
    /opt/nextcloud/config/pancrypticon.config.php && \
/bin/sed -i "s|MYSQL_PASSWORD|$MYSQL_PASSWORD|g" \
    /opt/nextcloud/config/pancrypticon.config.php && \
  /bin/sed -i "s|NEXTCLOUD_DOMAIN_NAME|$NEXTCLOUD_DOMAIN_NAME|g" \
      /etc/nginx/sites-enabled/default && \

# add the hidden domain to the trusted list
if [ ! -z $HIDDEN_DOMAIN_NAME ]; then
    /bin/sed -i "s|HIDDEN_DOMAIN_NAME|$HIDDEN_DOMAIN_NAME|g" \
        /opt/nextcloud/config/pancrypticon.config.php
fi && \

# Do not proceed until the database is available
# Sometimes the mariadb container is slow to execute docker-entrypoint.sh :(
while [[ -z $(echo "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = 'nextcloud';" | /usr/bin/mysql -h mariadb -u ${MYSQL_USER} -p${MYSQL_PASSWORD}) ]]; do
    /bin/echo "*** Waiting for presence of 'nextcloud' database..."
    /bin/sleep 10
done && \

cd /opt/nextcloud && \

if [ ! -f /opt/nextcloud-data/.installed ]; then
    # install nextcloud
    /bin/echo "*** Initializing new installation..."
    /usr/bin/sudo -u www-data php occ maintenance:install --database "mysql" \
    --database-name "nextcloud"  --database-user "$MYSQL_USER" \
    --database-host "mariadb" --database-pass "$MYSQL_PASSWORD" --admin-user "admin" \
    --admin-pass "$ADMIN_PASSWORD" --no-interaction && \

    # toggle some initial database parameters
    /bin/sed -i "s|ADMIN_EMAIL|$ADMIN_EMAIL|g" /opt/*.sql && \
    /bin/echo "*** Configuring initial app settings..."
    /usr/bin/mysql -f -h mariadb -u $MYSQL_USER -p"$MYSQL_PASSWORD" nextcloud < /opt/initial_settings.sql

    /usr/bin/sudo -u www-data php occ config:app:set activity notify_email_spreed --value='1'
    /usr/bin/sudo -u www-data php occ config:app:set core backgroundjobs_mode --value='cron'
    /usr/bin/sudo -u www-data php occ config:app:set core shareapi_allow_links --value='no'
    /usr/bin/sudo -u www-data php occ config:app:set core shareapi_allow_resharing --value='no'
    /usr/bin/sudo -u www-data php occ config:app:set core shareapi_only_share_with_group_members --value='yes'
    /usr/bin/sudo -u www-data php occ config:app:set files_antivirus av_mode --value='daemon'
    /usr/bin/sudo -u www-data php occ config:app:set files_antivirus av_host --value='clamav'
    /usr/bin/sudo -u www-data php occ config:app:set files_antivirus av_port --value=3310
    /usr/bin/sudo -u www-data php occ config:app:set files_antivirus av_infected_action --value='delete'
    /usr/bin/sudo -u www-data php occ config:app:set files_sharing incoming_server2server_share_enabled --value='no'
    /usr/bin/sudo -u www-data php occ config:app:set files_sharing outgoing_server2server_share_enabled --value='no'
    /usr/bin/sudo -u www-data php occ config:app:set files_sharing lookupServerEnabled --value='no'
    /usr/bin/sudo -u www-data php occ config:app:set files_sharing lookupServerUploadEnabled --value='no'
    /usr/bin/sudo -u www-data php occ config:app:set ojsxc serverType --value='external'
    /usr/bin/sudo -u www-data php occ config:app:set ojsxc xmppStartMinimized --value='false'
    /usr/bin/sudo -u www-data php occ config:app:set ojsxc iceSecret --value="${TURN_SECRET}"
    /usr/bin/sudo -u www-data php occ config:app:set ojsxc iceUrl --value="stun:${TURN_SERVER}"
    /usr/bin/sudo -u www-data php occ config:app:set ojsxc boshUrl --value="https://${NEXTCLOUD_DOMAIN_NAME}/http-bind/"
    /usr/bin/sudo -u www-data php occ config:app:set ojsxc xmppDomain --value="${NEXTCLOUD_DOMAIN_NAME}"
    /usr/bin/sudo -u www-data php occ config:app:set ojsxc xmppResource --value='pancrypticon'
    /usr/bin/sudo -u www-data php occ config:app:set ojsxc apiSecret --value="${OJSXC_TOKEN}"
    /usr/bin/sudo -u www-data php occ config:app:set password_policy enforceHaveIBeenPwned --value='1'
    /usr/bin/sudo -u www-data php occ config:app:set password_policy enforceNonCommonPassword --value='1'
    /usr/bin/sudo -u www-data php occ config:app:set password_policy minLength --value='11'
    /usr/bin/sudo -u www-data php occ config:app:set spreed stun_servers --value="[\"${NEXTCLOUD_DOMAIN_NAME}:5349\"]"
    /usr/bin/sudo -u www-data php occ config:app:set spreed turn_servers --value="[{\"server\":\"${NEXTCLOUD_DOMAIN_NAME}:5349\",\"secret\":\"${TURN_SECRET}\",\"protocols\":\"udp,tcp\"}]"
    /usr/bin/sudo -u www-data php occ config:app:set federation enabled --value='no'
    /usr/bin/sudo -u www-data php occ config:app:set federatedfilesharing enabled --value='no'

    if [[ "$ARCHITECTURE" == 'amd64' ]] || [[ "$ARCHITECTURE" == '' ]]; then
        /usr/bin/sudo -u www-data php occ config:app:set richdocuments wopi_url --value="https://${OFFICE_DOMAIN_NAME}"
    else
        /usr/bin/sudo -u www-data php occ config:app:set ownpad ownpad_etherpad_apikey --value="${ETHERPAD_API_TOKEN}"
        /usr/bin/sudo -u www-data php occ config:app:set ownpad ownpad_etherpad_cookie_domain --value="${NEXTCLOUD_DOMAIN_NAME}"
        /usr/bin/sudo -u www-data php occ config:app:set ownpad ownpad_etherpad_enable --value="yes"
        /usr/bin/sudo -u www-data php occ config:app:set ownpad ownpad_etherpad_host --value="https://${OFFICE_DOMAIN_NAME}"
        /usr/bin/sudo -u www-data php occ config:app:set ownpad ownpad_etherpad_useapi --value="yes"
    fi
else
    # run any needed updates/migrations
    /bin/echo "*** Running updates/migrations..."
    /usr/bin/sudo -u www-data php /opt/nextcloud/occ upgrade --no-interaction
    /usr/bin/sudo -u www-data php /opt/nextcloud/occ db:add-missing-indices
    # upconvert older schemas to use utf8mb4
    /usr/bin/sudo -u www-data php /opt/nextcloud/occ db:convert-mysql-charset
    # upconvert older schemas to use bigint
    /usr/bin/sudo -u www-data php /opt/nextcloud/occ db:convert-filecache-bigint
fi && \

# install additional apps
/bin/echo "*** Installing apps from appstore..."
/usr/local/sbin/install_apps.py

# set the location for the 'privacy' app
/usr/local/sbin/geoip.py

# As of spreed 4.99.0 whatever you set for "stun_servers" gets over-written when
# the app is installed, so we must over-over-write it if this is a new install
if [ ! -f /opt/nextcloud-data/.installed ]; then
    /usr/bin/sudo -u www-data php occ config:app:set spreed stun_servers --value="[\"${NEXTCLOUD_DOMAIN_NAME}:5349\"]"
fi

# (en|dis)able built-in apps
/bin/echo "*** Configuring built-in apps..."
/usr/bin/sudo -u www-data php occ app:disable firstrunwizard && \
/usr/bin/sudo -u www-data php occ app:disable sharebymail && \
/usr/bin/sudo -u www-data php occ app:disable survey_client && \
/usr/bin/sudo -u www-data php occ app:disable theming && \
/usr/bin/sudo -u www-data php occ app:disable updatenotification && \
/usr/bin/sudo -u www-data php occ app:enable files_retention && \
/usr/bin/sudo -u www-data php occ app:enable logreader && \
if [[ -z $SELF_HOSTED || $SELF_HOSTED == '0' || $SELF_HOSTED == 'False' ]]; then
  /usr/bin/sudo -u www-data php occ app:enable terms_of_service
fi

# run the updater in case this was an upgrade
#/usr/bin/sudo -u www-data php occ maintenance:update:htaccess && \

# update the installed version file
/bin/echo -n "${NEXTCLOUD_VERSION}" > /opt/nextcloud-data/.installed
/bin/echo "*** Installation/updates complete..."
