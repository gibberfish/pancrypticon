#!/usr/bin/python3

import json, mysql.connector, requests, re, subprocess, sys, time
from distutils.util import strtobool
from operator import itemgetter
from os import path, environ, chdir

arch = environ.get("ARCHITECTURE", "amd64")
apps = [
    "calendar",
    "circles",
    "contacts",
    "data_request",
    "deck",
    "drop_account",
    "event_update_notification",
    "federation" "files_accesscontrol",
    "files_antivirus",
    "files_automatedtagging",
    "files_retention",
    "groupfolders",
    "news",
    "ojsxc",
    "spreed",
    "tasks",
    "twofactor_totp",
]
apps_amd64 = ["richdocuments"]
apps_armhf = ["ownpad"]
apps_arm64 = apps_armhf

try:
    self_hosted = bool(strtobool(environ.get("SELF_HOSTED", "False")))
except ValueError:
    self_hosted = False

# if SELF_HOSTED is False, install the TOS app
if not self_hosted:
    apps.append("terms_of_service")

r = requests.get(
    "https://apps.nextcloud.com/api/v1/platform/%s/apps.json"
    % environ["NEXTCLOUD_VERSION"]
)

if r.status_code == requests.codes.ok:
    applist = r.json()

    for app in applist:
        chdir("/opt/nextcloud/")
        app_path = "/opt/nextcloud/apps/%s" % app["id"]
        if not path.isdir(app_path):
            if (
                app["id"] in apps
                or (arch == "amd64" and app["id"] in apps_amd64)
                or (arch == "arm64" and app["id"] in apps_arm64)
                or (arch == "armhf" and app["id"] in apps_armhf)
            ):
                releases = sorted(app["releases"], key=itemgetter("created"))
                if len(releases):
                    latest = releases[-1]
                    print(
                        "App '%s' not found: installing version %s ..."
                        % (app["id"], latest["version"])
                    )
                    data = requests.get(latest["download"])
                    filename = "%s-%s.tar.gz" % (app["id"], latest["version"])
                    with open(filename, "wb") as f:
                        f.write(data.content)
                    subprocess.call(
                        ["/bin/tar", "-C", "/opt/nextcloud/apps", "-xpf", filename]
                    )
                    sys.stdout.flush()
                    subprocess.call(["/bin/chown", "-R", "www-data:www-data", app_path])
                    sys.stdout.flush()
                    subprocess.call(
                        [
                            "/usr/bin/sudo",
                            "-u",
                            "www-data",
                            "php",
                            "occ",
                            "app:enable",
                            app["id"],
                        ]
                    )
                    subprocess.call(["/bin/rm", "-f", filename])

if not self_hosted:
    """
    Check to see if there is already a TOS defined, and if not, add one
    """
    db = mysql.connector.connect(
        host="mariadb",
        user=environ["MYSQL_USER"],
        passwd=environ["MYSQL_PASSWORD"],
        database="nextcloud",
    )
    cursor = db.cursor(buffered=True)
    cursor.execute("SELECT * FROM oc_termsofservice_terms")
    if cursor.rowcount == 0:
        insert = "INSERT INTO oc_termsofservice_terms \
            (country_code,language_code, body) VALUES (%s, %s, %s)"
        cursor.execute(
            insert,
            (
                "--",
                "en",
                "By using this site, you are indicating that you have read and \
agree to the Terms of Service, which are available at \
https://gibberfish.org/terms/",
            ),
        )
        db.commit()
