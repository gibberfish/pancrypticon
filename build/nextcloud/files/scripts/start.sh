#!/bin/bash

# install/upgrade nextcloud if not already current
/usr/local/sbin/install.sh && \

/etc/init.d/cron start && \

# start nginx
/usr/local/sbin/php-fpm --daemonize --force-stderr && \
/usr/sbin/nginx -g 'daemon off;'
