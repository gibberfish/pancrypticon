#!/bin/bash

if [[ ! -d /etherpad-lite ]]; then
    cd /
    /usr/sbin/adduser --quiet --disabled-login etherpad
    /usr/bin/git clone --branch master git://github.com/ether/etherpad-lite.git
    cd etherpad-lite
    /bin/echo "$ETHERPAD_API_TOKEN" > APIKEY.txt
    /bin/cp /tmp/settings.json .
    /bin/sed -i "s|MYSQL_USER|$MYSQL_USER|g" settings.json
    /bin/sed -i "s|MYSQL_PASSWORD|$MYSQL_PASSWORD|g" settings.json
    /bin/sed -i "s|LANGUAGE|$LANGUAGE|g" settings.json
    /usr/local/bin/npm install \
        ep_authorship_toggle \
        ep_disable_custom_scripts_and_styles \
        ep_hide_referrer \
        ep_image_upload \
        ep_print ep_headings2 \
        ep_spellcheck
    /bin/chown -R etherpad.etherpad .
fi

# Do not proceed until the database is available
while [ `/bin/nc -z mariadb 3306` ]; do
    /bin/echo "*** Waiting for database container to initialize..."
    /bin/sleep 3
done && \

cd /etherpad-lite && sudo -u etherpad NODE_ENV=production bin/run.sh
