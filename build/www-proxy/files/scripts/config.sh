#!/bin/bash

/bin/mkdir -p /var/www/html/custom_50x/
/bin/cp -p /tmp/custom_50x.html /var/www/html/custom_50x/
/bin/cp -p /tmp/gibberfish-logo.png /var/www/html/custom_50x/
/bin/cp -p /tmp/pancrypticon-logo.png /var/www/html/custom_50x/

cd /etc/nginx/sites-enabled/ && \
/bin/ln -sf /etc/nginx/sites-available/default && \
/bin/ln -sf /etc/nginx/sites-available/default-ssl && \
if [[ "$ARCHITECTURE" == 'amd64' ]] || [ -z $ARCHITECTURE ]; then
    /bin/ln -sf /etc/nginx/sites-available/collabora
else
    /bin/ln -sf /etc/nginx/sites-available/etherpad
fi

/bin/sed -i "s|NEXTCLOUD_DOMAIN_NAME|$NEXTCLOUD_DOMAIN_NAME|g" \
    /etc/nginx/sites-available/* && \
/bin/sed -i "s|OFFICE_DOMAIN_NAME|$OFFICE_DOMAIN_NAME|g" \
    /etc/nginx/sites-available/* && \
/bin/sed -i "s|HIDDEN_DOMAIN_NAME|$HIDDEN_DOMAIN_NAME|g" \
    /etc/nginx/sites-available/* && \
/bin/sed -i "s|NEXTCLOUD_THEME|$NEXTCLOUD_THEME|g" \
    /etc/nginx/sites-available/* && \
/usr/bin/find /var/www/html -name *.html -exec \
    /bin/sed -i "s|NEXTCLOUD_THEME|$NEXTCLOUD_THEME|g" {} \;

if [ $TESTING ]; then
    /bin/sed -i "s|SSLCertificateChainFile|#SSLCertificateChainFile|g" \
        /etc/nginx/sites-available/*
else
    /bin/sed -i "s|#SSLCertificateChainFile|SSLCertificateChainFile|g" \
        /etc/nginx/sites-available/*
fi
