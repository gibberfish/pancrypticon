#!/bin/bash

# use fake cert for testing
if [ $TESTING ]; then
    /bin/mkdir -p /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME && \
    /bin/cp /etc/ssl/certs/ssl-cert-snakeoil.pem \
        /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/fullchain.pem && \
    /bin/cp /etc/ssl/certs/ssl-cert-snakeoil.pem \
        /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/cert.pem && \
    /bin/cp /etc/ssl/private/ssl-cert-snakeoil.key \
        /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/privkey.pem && \
    /bin/ln -s /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/fullchain.pem \
        /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/chain.pem && \
    /bin/chmod 600 /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/*

## generate and install a letsencrypt cert
elif [ ! -f /etc/letsencrypt/.installed ] || \
     [[ `/bin/cat /etc/letsencrypt/.installed` -lt 1542259700 ]]; then
        # Use legacy defaults if variables are missing from the environment
        if [ -z $OFFICE_DOMAIN_NAME ]; then
            OFFICE_DOMAIN_NAME="office.${NEXTCLOUD_DOMAIN_NAME}"
        fi
        if [ -z $CONFERENCE_DOMAIN_NAME ]; then
            CONFERENCE_DOMAIN_NAME="conference.${NEXTCLOUD_DOMAIN_NAME}"
        fi
        /usr/sbin/nginx -c /etc/nginx/nginx-certbot.conf && \
        /usr/bin/certbot \
            certonly \
            --webroot \
            --webroot-path /var/www/html \
            --email "$ADMIN_EMAIL" \
            --agree-tos \
            --hsts \
            --rsa-key-size 4096 \
            --expand \
            --noninteractive \
            --domain $NEXTCLOUD_DOMAIN_NAME \
            --domain $OFFICE_DOMAIN_NAME \
            --domain $CONFERENCE_DOMAIN_NAME && \
            /bin/echo `date +%s` > /etc/letsencrypt/.installed && \
        /usr/sbin/nginx -c /etc/nginx/nginx-certbot.conf -s quit
else
    /usr/bin/certbot renew --non-interactive
fi

# generate new Diffie-Helmann parameters
if [ ! -f /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/dhparams.pem ]; then
    /usr/sbin/rngd -r /dev/urandom
    /usr/bin/openssl dhparam -out \
        /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/dhparams.pem 2048 && \
    chmod 600 /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/dhparams.pem && \
    chown www-data /etc/letsencrypt/live/$NEXTCLOUD_DOMAIN_NAME/dhparams.pem
fi
