#!/bin/bash

# start cron
/etc/init.d/cron start

/usr/local/sbin/config.sh && \
/usr/local/sbin/certbot.sh && \
/usr/sbin/nginx -g 'daemon off;'
