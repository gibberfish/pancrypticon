#!/bin/bash

EXTERNAL_IP=`/usr/bin/curl https://ipv4.nsupdate.info/myip`
INTERNAL_IP=`ip a | grep 'inet 172' | awk '{print $2}' | awk -F/ '{print $1}'`

/usr/bin/sec --detach --conf=/etc/sec/letsencrypt.rules \
    --input=/etc/letsencrypt/letsencrypt.log

/bin/sed -i "s|NEXTCLOUD_DOMAIN_NAME|$NEXTCLOUD_DOMAIN_NAME|g" \
    /etc/turnserver.conf
/bin/sed -i "s|TURN_SECRET|$TURN_SECRET|g" /etc/turnserver.conf
/bin/sed -i "s|EXTERNAL_IP|$EXTERNAL_IP|g" /etc/turnserver.conf
/bin/sed -i "s|INTERNAL_IP|$INTERNAL_IP|g" /etc/turnserver.conf

/usr/bin/turnserver -c /etc/turnserver.conf -v
