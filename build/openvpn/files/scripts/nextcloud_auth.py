#!/usr/bin/python
"""
Authenticate user against Nextcloud via its REST API
Returns 0 if auth is successful
"""

import os, requests

username = os.environ.get('username')
password = os.environ.get('password')
hostname = 'NEXTCLOUD_DOMAIN_NAME'
url = "http://nextcloud/ocs/v2.php/cloud/user"
r = requests.get(url, auth=(username, password), headers={'OCS-APIRequest': 'true', 'Host': '%s' % hostname})

if r.status_code == requests.codes.ok:
    print('Authenticated: True')
    exit(0)
print('Authenticated: False')
exit(1)
