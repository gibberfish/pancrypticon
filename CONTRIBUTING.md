Thanks for taking an interest in Pancrypticon. We develop this platform
primarily to support our hosting platform, but in true open source fashion,
we've also designed the software so that it can be run by anyone with a linux PC
and a little bit of technical know-how.

We are a small, volunteer team, and we welcome community input and
contributions!

Here is a very short list of suggestions to keep in mind when contributing to
the project:

1. When submitting a merge request, please give as much detail as possible not
only about what your change entails, but the reasons which justify making the
change. In some cases these may be obvious, and in others less so. The more
clear and communicative you can be up front, the easier it will be for us to
understand the value of your contribution.

2. If you plan to add a brand new feature, or make major changes in the
functionality of existing features, please contact us first so we can discuss
whether or not those changes fit into our vision for the project.

We can be reached via email at info@gibberfish.org, as well as individually via
the contact methods listed on our website at https://gibberfish.org/contact-us

Cheers!