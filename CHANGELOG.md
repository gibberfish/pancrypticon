# Changelog

## [Unreleased]

## [20200219.0] - 2020-02-19
### Fixed
- Switch user in collabora build to install netcat

### Changed
- Bump Nextcloud to 16.0.8
- Drop TLSv1.1 support and update cipher suites

## [20200218.0] - 2020-02-18
### Changed
- Bump Nextcloud to 16.0.8
- Drop TLSv1.1 support and update cipher suites

## [20191102.0] - 2019-11-02
### Added
- Add `CHANGELOG.md`

### Changed
- Force use of master key in pre-commit hook
- Bump Nextcloud to 16.0.5
- Fixed nginx config (see: https://nextcloud.com/blog/urgent-security-issue-in-nginx-php-fpm/)

### Fixed
- Fix deps no longer available in debian buster (php-fpm base image)

## [20190615.0] - 2019-06-15
### Changed
- Merge to reconcile divergence from development branch

### Fixed
- Disable http2 on port 80 default server because certbot can't handle it


[Unreleased]: https://gitlab.com/gibberfish/pancrypticon/compare/20200219.0...staging
[20200219.0]: https://gitlab.com/gibberfish/pancrypticon/compare/20200218.0...20200219.0
[20200218.0]: https://gitlab.com/gibberfish/pancrypticon/compare/20191102.0...20200218.0
[20191102.0]: https://gitlab.com/gibberfish/pancrypticon/compare/20190615.0...20191102.0
[20190615.0]: https://gitlab.com/gibberfish/pancrypticon/compare/20190522.0...20190615.0
