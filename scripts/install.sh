#!/bin/bash

if [ -z $ARCHITECTURE ]; then
    ARCHITECTURE=`/usr/bin/dpkg --print-architecture`
fi

if `egrep -q '^[8-9]\.?' /etc/debian_version`; then
    echo "*** Debian 8/9 detected."
    DISTRO='debian'
elif `egrep -q '^(jessie|stretch)\/?' /etc/debian_versions`; then
    echo "*** Ubuntu 16/17 detected."
    DISTRO='ubuntu'
else
    echo \
        "This script currently works for Debian 8/9 and Ubuntu 16/17. If you are
using a different distro, please follow the manual instructions in the
wiki:
        https://gitlab.com/gibberfish/pancrypticon/wikis/install"
    exit 0
fi

echo "*** Installing docker-ce and prerequisites..."
sudo apt-get -y install tor && \
systemctl start tor && \
sleep 10 && \
sudo torsocks apt-get -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg2 \
    software-properties-common

    torsocks curl -fsSL https://download.docker.com/linux/$DISTRO/gpg | sudo apt-key add -
    sudo apt-key fingerprint 0EBFCD88
    sudo add-apt-repository \
        echo "deb [arch=${ARCHITECTURE}] https://download.docker.com/linux/$DISTRO \
        $(lsb_release -cs) \
        stable"
    sudo torsocks apt-get -y update
    sudo torsocks apt-get install -y \
        docker-ce \
        git \
        gnupg2 \
        pwgen \
        python-pip \
        libssl1.0.0

echo "*** Installing docker-compose..."
sudo torsocks pip install --upgrade docker-compose

echo "*** Setting up docker..."
sudo mkdir -p /opt/pancrypticon /srv/pancrypticon
echo -n '{"graph": "/opt/pancrypticon/docker","storage-driver": "overlay2"}' > \
    /etc/docker/daemon.json
sudo systemctl start docker
sudo systemctl enable docker

echo "*** Downloading pancrypticon..."
cd /opt/pancrypticon
sudo torsocks git clone https://gitlab.com/gibberfish/pancrypticon.git
cd pancrypticon

echo "*** Importing Gibberfish Security PGP key..."
gpg --import security-pubkey.asc

echo "*** Verifying code integrity..."
gpg --verify checksums.json.asc
scripts/verify-checksums.py

echo "*** Copying initial database schema files..."
chown 999 initdb.d
sudo cp build/ejabberd/files/initdb.d/* initdb.d/
sudo cp build/etherpad/files/initdb.d/* initdb.d/
sudo cp settings.env.example settings.env

echo "*** Generating random secrets..."
TURN_SECRET=`pwgen -s 64 -n 1`
OJSXC_TOKEN=`pwgen -s 23 -n 1`
MYSQL_ROOT_PASSWORD=`pwgen -s 32 -n 1`
sed -i "s|^TURN_SECRET=.*|TURN_SECRET=$TURN_SECRET|" settings.env
sed -i "s|^OJSXC_TOKEN=.*|OJSXC_TOKEN=$OJSXC_TOKEN|" settings.env
sed -i "s|^MYSQL_ROOT_PASSWORD=.*|MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD|" \
    settings.env
sed -i "s|^ARCHITECTURE=.*|ARCHITECTURE=$ARCHITECTURE|" settings.env

echo "*** Installation complete!"
