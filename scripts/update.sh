#!/bin/bash

CONFIG_DB="/home/daygate/daygate/db.sqlite3"
UPDATES_ENABLED=`echo "SELECT value FROM deploy_config WHERE key = 'auto_update_pancrypticon';" | \
    sqlite3 $CONFIG_DB`
if [ -z $ARCHITECTURE ]; then
    ARCHITECTURE=`/usr/bin/dpkg --print-architecture`
fi

# remove old containers, images, etc
docker system prune --all --force --volumes

if [[ $UPDATES_ENABLED == 'False' || $UPDATES_ENABLED == '' ]]; then
    exit 0
fi

cd /opt/pancrypticon/pancrypticon && \

GIT_BRANCH=`git rev-parse --abbrev-ref HEAD`

# set the correct options based on system arch
compose_cmd="/usr/local/bin/docker-compose --file docker-compose.yml"
arch_overlay="docker-compose-${ARCHITECTURE}.yml"
if [ -f ${arch_overlay} ]; then
    compose_cmd="${compose_cmd} --file ${arch_overlay}"
fi

# check for updates
torsocks git remote update 2>&1 > /dev/null
if [[ `git status --untracked-files=no 2>&1 | \
        grep -c "Your branch is behind"` -eq "1" \
]];
then
    torsocks git pull && sudo scripts/verify-checksums.py || exit $?
    ${compose_cmd} build --pull --no-cache
fi

# start/update containers
${compose_cmd} up -d --remove-orphans
# run any needed upgrades on the db/tables
sleep 30 && \
source settings.env && \
docker exec -it mariadb mysql_upgrade -p"${MYSQL_ROOT_PASSWORD}"
# remove old containers, images, etc
docker system prune --all --force --volumes
docker rmi $(docker images -f "dangling=true" -q)
